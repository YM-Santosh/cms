<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable=['menu_title','menu_status','menu_order'];

    public function pages()
    {
        return $this->hasMany('App\Page');
    }
}
