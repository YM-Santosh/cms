<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Menu;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        $menus=Menu::all();
//
//         $submenu= DB::table('menus')
//            ->join('pages','menus.id','=','pages.menu_id')
//            ->select(
//                'menus.*',
//                'pages.id as page_id',
//                'pages.page_title as page_title',
//                'pages.menu_id as menu_id'
//            )
//            ->get   ();
//
//
//        view::share(compact('menus','submenu'));

        $menu = Menu::with('pages')->get();
        view::share('menus',$menu);
    }

    public static function pageExist($id){
         $pages=DB::table('pages')
            
            ->select('pages.page_title as page_title')
            ->where('pages.menu_id',$id)->get();
        return $pages;
    }
    public static function menucount($id){
       $count= DB::table('menus')
            ->join('pages','menus.id','=','pages.menu_id')
            ->select(
                'menus.id as menu_id'
            )
            ->where('menu_id',$id)
            ->count();
        return $count;
    }

   

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
