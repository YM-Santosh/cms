<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded =['id','created_at',"updated_at"];
    public function menu()
    {
        return $this->belongsTo('App\Menu');
    }
}
