<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminController extends Controller
{
    public function index(){
        return view('admin.pages.dashboard');
    }

    public function logOut(){
        Auth::logout();
        return redirect()->route('login');

    }
}
