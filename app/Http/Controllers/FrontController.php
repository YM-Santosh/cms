<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Page;

class FrontController extends Controller
{
//    public function frontPage(){
//        $menus=Menu::all()->load('pages');
////        echo "<pre>";
////        print_r($value);exit;
//
//        return view('front.pages.webpage')->compact('menus');
//
//
//    }

    public function index(){
        $page=Page::where('page_title','Hello')->first();

        return view('front.pages.webpage')->with('page',$page);

    }

    public function menuPage(Request $request){
        $page=Page::where('menu_id',$request->id)->get();
         return view('front.pages.webpage')->with('page',$page[0]);
    }

    public function contentPage(Request $request){
//        return $request->id;
        $page = Page::find($request->id);
        
        return view('front.pages.webpage')->with('page',$page);
    }
}
