<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;


class UserController extends Controller
{
    public function addMenu(){


       
        return view('user.menu.add_menu');
    }

    public function addMenuAction(Request $request){
        $this->validate($request,
            [
                'menu'=>'required|min:3',
                'order'=>'required|numeric',
                's'=>'required'
            
            ]
        );


         $data=[
             'menu_title'=>$request->menu,
             'menu_order'=>$request->order,
             'menu_status'=>$request->s
         ];
        $result=Menu::create($data);

        if ($result) {
            return redirect()->route('add-menu')->with('success', 'Menu added Successfully');
        } else {
            return redirect()->route('add-menu')->with('error', 'Menu could not be added');
        }


    }
}
