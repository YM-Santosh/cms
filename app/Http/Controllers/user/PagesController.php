<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Page;
use Illuminate\Support\Facades\DB;
use Image;

class PagesController extends Controller
{
    public function addPages(){



        $menus=Menu::all();

        return view('user.pages.add_pages')->with('menus',$menus);
    }

    public function addPagesAction(Request $request){
//        return $request->all();

        $data = [
            'page_title' => $request->title,
            'menu_id' => $request->parent_id,
            'page_desc' => $request->desc,
            'page_status'=>$request->s,

        ];

        if($request->hasFile('image')){
            $image=$request->file('image');
//            echo "<pre>";
//            print_r($image);exit;

            $name=time().'.'.$image->getClientOriginalExtension();
            Image::make($image)->resize(500,null,function($constraints){$constraints->aspectRatio();})->crop(300,300)->save(public_path().'/uploads/pages/'.$name);
            $data['page_image']=$name;
        }

        $result = Page::create($data);

        if ($result) {
            return redirect()->route('add-pages')->with('success', 'Page created successfully');

        } else {
            return redirect()->route('add-pages')->with('error', 'Could Not create page');

        }
    }
}
