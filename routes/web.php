<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
/*
 * Login By Using Laravel's Original Authentication
 */

        Route::get('/', function () {
            return view('welcome');
        });

        Auth::routes();

        Route::get('/home', 'HomeController@index');

        Route::group(['prefix'=>'admin','namespace'=>'admin'],function(){
           Route::get('/','AdminController@index');
            Route::get('/dashboard','AdminController@index')->name('dash');
        });

        Route::get('/logout','admin\AdminController@logOut')->name('logout');


/*
 * Creating a CMS Back End
 */


    Route::group(['prefix'=>'user','namespace'=>'user'],function(){
        Route::get('add-menu','UserController@addMenu')->name('add-menu');
        Route::post('add-menu','UserController@addMenuAction');

        Route::group(['prefix'=>'menu'],function(){
           Route::get('add-pages','PagesController@addPages')->name('add-pages');
            Route::post('add-pages','PagesController@addPagesAction');

        });
    });

    Route::group(['prefix'=>'front'],function(){
        Route::get('/','FrontController@index');
        Route::get('web-page/{id?}','FrontController@contentPage');
        Route::get('menu/{id}','FrontController@menuPage');
    });
