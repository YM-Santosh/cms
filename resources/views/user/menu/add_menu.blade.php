@extends('layouts.master')

@section('title')
    Add Menus
@endsection

@section('content')
    <div class="content">
        <h1><i class="fa fa-plus"></i> ADD MENU</h1>
        <hr>
        @include('admin.common.message')
        <row>
            <form action="{{route('add-menu')}}" method="post">

                {{csrf_field()}}

                <div class="form-group col-md-6">
                    <label>Add Menu</label>
                    <input type="text" name="menu" value="{{old('menu')}}" class="form-control">
                </div>

                <div class="col-md-3 form-group">
                    <label for="">Order</label>
                    <select name="order" id="order" class="form-control">
                        @for($i=1;$i<=10;$i++)
                        <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>

                </div>


                <div class="form-group">
                    <label for="">Status</label><br>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary">
                            <input type="radio" name="s" id="enable" autocomplete="off" value="enabled"> Enable
                        </label>
                        <label class="btn btn-danger">
                            <input type="radio" name="s" id="disable" autocomplete="off" value="disabled"> Disable
                        </label>
                    </div>
                </div>

                <div class="clearfix">
                    <div class="form-group col-md-1">
                        <input type="submit" value="Add Menu" class="btn btn-success">
                    </div>
                </div>

            </form>
        </row>
    </div>
@endsection
