@extends('layouts.master')

@section('title')
    Add Pages Information
@endsection

@section('content')
    <div class="content">
        <h1><i class="fa fa-plus"></i> ADD PAGES </h1>
        <hr>
        <form action="{{route('add-pages')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="row">
                <div class="col-md-6 form-group">
                    <label for=""><h3>Menu</h3></label>
                    <select name="parent_id" id="menus" class="form-control">
                        <option value="" disabled>-- Choose Menu --</option>
                        @foreach ($menus as $menu)
                            <option value="{{$menu->id}}">{{$menu->menu_title}}</option>
                        @endforeach
                    </select>

                </div>

                <div class="col-md-6 form-group">
                    <label for=""><h3>Title</h3></label>
                    <input type="text" name="title" class="form-control">

                </div>

                <div class="form-group col-md-6">
                    <label for="">Status</label><br>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary">
                            <input type="radio" name="s" id="enable" autocomplete="off" value="enabled"> Enable
                        </label>
                        <label class="btn btn-danger">
                            <input type="radio" name="s" id="disable" autocomplete="off" value="disabled"> Disable
                        </label>
                    </div>
                </div>

                <div class="col-md-3 form-group">
                    <label for="">Order</label>
                    <select name="order" id="order" class="form-control">
                        @for($i=1;$i<=10;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>

                </div>

                <div class="form-group col-md-6">
                    <label>Upload Image</label>
                    <input type="file" name="image" value="{{old('image')}}" class="form-control">
                </div>


                <div class="col-md-12 form-group">
                    <label for="">Description</label>
                    <textarea rows="5" name="desc" class="form-control"></textarea>

                </div>
                <div class="clearfix">
                <div class="form-group col-md-1">
                    <input type="submit" value="Add Page" class="btn btn-success">
                </div>
                </div>

            </div>

        </form>


    </div>
    <script>
        CKEDITOR.replace('desc');

    </script>




@endsection