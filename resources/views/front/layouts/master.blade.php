@include('front.common.head')
@include('front.common.nav')
@include('front.common.foot')


@yield('head')
@yield('nav')

@yield('content')


@yield('foot')
