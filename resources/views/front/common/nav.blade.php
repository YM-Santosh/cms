@section('nav')
    <div class="container">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">

                    <a class="navbar-brand" href="#">Content Management System</a>
                </div>


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">




                    <ul class="nav navbar-nav">
                        @foreach($menus as $menu)


                            @if ($menu->pages->count() && ($menu->menu_title!=$menu->pages[0]->page_title))
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true"
                                       aria-expanded="false">{{$menu->menu_title}} <span class="caret"></span></a>

                                    <ul class="dropdown-menu">
                                        @foreach ($menu->pages as $page)
                                            <li>
                                                <a href="{{url('front/web-page/'.$page->id)}}">{{$page->page_title}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @else

                                <li><a href="{{url('front/menu/'.$menu->id)}}">{{$menu->menu_title}}</a></li>
                            @endif

                        @endforeach


                    </ul>


                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
@endsection