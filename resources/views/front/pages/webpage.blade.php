
@extends('front.layouts.master')
@section('content')
<div class="container">
    <div class="content">
        <div class="row thumbnail">
          <div class="col-md-12">
              <h1>{{$page->page_title}}</h1>
          </div>

            <div class="thumbnail col-md-7">
                <p>{!! $page->page_desc !!}</p>
            </div>

            <div class="img-thumbnail col-md-5">
                <img src="{{ asset("uploads/pages/$page->page_image") }}" alt="{{ $page->page_title }}" >;
            </div>


        </div>
    </div>
</div>
@endsection